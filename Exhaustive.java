public class Exhaustive {
    public static boolean canPartition(int[] num) {
        int sum = 0;
        for (int i = 0; i < num.length; i++) {
            sum += num[i];
        }
        if (sum % 2 != 0) {
            return false;
        }
        return findPartition(num, sum / 2, 0);
    }
    public static boolean findPartition(int[] num, int sum, int currentIndex) {
        if (sum == 0) {
            return true;
        }

        if (num.length == 0 || currentIndex >= num.length) {
            return false;
        }

        if (num[currentIndex] <= sum) {
            if (findPartition(num, sum - num[currentIndex], currentIndex + 1)) {
                return true;
            }
        }
        return findPartition(num, sum, currentIndex + 1);
    }

    public static void main(String[] args) {
        int[] num = { 6, 12, 23, 4, 1 };
        System.out.println(canPartition(num));
    }
}
