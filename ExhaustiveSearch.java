

public class ExhaustiveSearch {
    public static void main(String[] args) {
        fourAB("");
    }
    public static void fourAB(String soFar) {
        if (soFar.length() == 4) {
            System.out.println(soFar);
        } else {
            fourAB(soFar + "a");
            fourAB(soFar + "b");
        }
    }
}
